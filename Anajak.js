let exp = ["Network maintenance according to customer needs (Product Cisco).","Analyze the cause and solve network problems.","Implement the Network system for customers and design the network structure.","Support operation support / Trouble shooting to the assigned Site.","Support Pre Sales work with Sales team to receive Requirement with new customers about Project Network.","Maintain Service Level Agreement (SLA) in accordance with the agreement."];
let aow = ["LAN/WAN","TCP/IP","Router, Switch","Network Information"];
let skill = ["HTML","CSS","javascript/ReactJS","C","Firebase","Netlify"];

let exphtml = `<ul>`;
for (const x of exp) {
    exphtml += `<li>${x}</li>`;
  }
exphtml += `</ul>`;
document.getElementById("exp").innerHTML = exphtml;
let aowhtml = `<ul>`;
for (const x of aow) {
    aowhtml += `<li>${x}</li>`;
  }
aowhtml += `</ul>`;
document.getElementById("aow").innerHTML = aowhtml;
let skillhtml = `<ul>`;
for (const x of skill) {
    skillhtml += `<li>${x}</li>`;
  }
skillhtml += `</ul>`;
document.getElementById("skill").innerHTML = skillhtml;